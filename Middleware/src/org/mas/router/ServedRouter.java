/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.router;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mas.common.IMASName;
import org.mas.common.address.MasAddress;
import org.mas.common.node.INodeAnnouncer;
import org.mas.common.node.INodeListener;
import org.mas.message.Message;
import org.mas.message.system.SystemMessage;
import org.mas.message.system.SystemMessageType;
import org.mas.message.user.UserMessage;
import org.mas.sockets.ISocketListener;
import org.mas.sockets.client.ClientSocket;
import org.mas.sockets.router.RouterServerSocket;

/**
 * Implementation of a router that uses sockets
 *
 * @author Sam Moore
 */
public class ServedRouter implements ISocketListener, INodeAnnouncer
{

    private final RouterServerSocket serverSocket;
    private final ConcurrentHashMap<String, ClientSocket> portals;
    private final CopyOnWriteArrayList<String> routes;
    private final List<INodeListener> nodeListeners;
    private String masName;

    public ServedRouter()
    {
        serverSocket = new RouterServerSocket(1024);
        serverSocket.registerSocketListener(this::receiveMessage);

        if (!serverSocket.initSocket())
        {
            System.err.println("Failed to start server socket!");
        }

        portals = new ConcurrentHashMap<>();
        routes = new CopyOnWriteArrayList<>();
        nodeListeners = new ArrayList<>();
        masName = this.getClass().
                getSimpleName() + " - " + IMASName.getRandomChars();
    }

    public boolean registerPortal(String key, ClientSocket portal)
    {
        //If key is null don't register portal
        if (key == null)
        {
            return false;
        }
        //If portal with same name is registered don't register portal again
        if (portals.contains(key))
        {
            return false;
        }
        //Else add portal to portal hashmap
        portals.put(key, portal);
        return true;
    }

    public boolean deregisterPortal(String portal)
    {
        //deregister portal if portal actually exists in map
        if (portals.contains(portal))
        {
            portals.remove(portal);
            return true;
        }
        else
        {
            return false;
        }

    }

    public boolean registerRoute(String route)
    {
        if (route != null)
        {
            routes.add(route);
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean deregisterRoute(String route)
    {
        if (route != null)
        {
            routes.remove(route);
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public void receiveMessage(Message msg)
    {
        if (msg == null)
        {
            return;
        }

        nodeListeners.forEach(listener ->
        {
            listener.receiveNodeMessage(this, msg);
        });

        if (msg instanceof SystemMessage)
        {
            handleSysMessage((SystemMessage) msg);
        }
        else if (msg instanceof UserMessage)
        {
            handleUserMessage((UserMessage) msg);
        }
        else
        {
            //A Message cannot be instanciated so something is very wrong
            System.err.println("Corrupted Message Received - Check Log");
            Logger.getLogger(ServedRouter.class.getName()).
                    log(Level.WARNING, msg.toString());
        }

        String name = msg.getDestination().
                getPortal();

        portals.get(name).
                enqueueMessage(msg);

    }

    private void handleSysMessage(SystemMessage msg)
    {
        String parameter = msg.getParameter();

        switch (msg.getOperation())
        {
            case JOINING:
                registerRoute(parameter);
                break;
            case LEAVING:
                deregisterRoute(parameter);
                break;
            case WHOIS:
                SystemMessage reply = new SystemMessage(SystemMessageType.RESPONSE, masName, msg.
                                                        getSource(), new MasAddress(masName, null, null));
                //sendMessage(msg);
                break;
        }
    }

    private void handleUserMessage(UserMessage msg)
    {
        String portal = msg.getDestination().
                getPortal();

        if (portals.containsKey(portal))
        {
            portals.get(portal).
                    enqueueMessage(msg);
        }
    }

    @Override
    public boolean registerNodeListener(INodeListener listener)
    {
        return nodeListeners.add(listener);
    }

    @Override
    public boolean deregisterNodeListener(INodeListener listener)
    {
        return nodeListeners.remove(listener);
    }

    @Override
    public void setMasName(String name)
    {
        masName = name;
    }

    @Override
    public String getMasName()
    {
        return masName;
    }
}
