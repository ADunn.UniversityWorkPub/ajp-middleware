/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.agent;

/**
 *
 * @author Adam Dunn
 */
public interface IMessageAnnouncer
{

    /**
     * Adds listener to the list of object to notify when a new message is
     * received
     *
     * @param listener The listener to add to the list
     * @return True if the listener was added
     */
    public boolean registerListener(IMessageListener listener);

    /**
     * Removes a listener from the list of object to notify when a new message
     * is received
     *
     * @param listener The listener to remove from the list
     * @return True if the listener was removed
     */
    public boolean deregisterListener(IMessageListener listener);
}
