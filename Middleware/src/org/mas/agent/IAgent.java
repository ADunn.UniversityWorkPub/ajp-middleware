/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.agent;

import org.mas.common.IMASName;
import org.mas.message.user.UserMessage;
import org.mas.portal.IPortal;

/**
 * Defines operations to interact with in and out queues within an agent
 *
 * @author Adam Dunn
 */
public interface IAgent extends IMASName
{

    /**
     * Add a message to the outgoing queue of this agent
     *
     * @param msg The message to add to the queue
     * @return True if the message was successfully added
     */
    public boolean putOutMessage(UserMessage msg);

    /**
     * Returns the head of the outgoing queue
     *
     * @return The message at the head of the outgoing queue
     */
    public UserMessage getOutMessage();

    /**
     * Add a message to the incoming queue of this agent
     *
     * @param msg The message to add to the queue
     * @return True if the message was successfully added
     */
    public boolean putInMessage(UserMessage msg);

    /**
     * Returns the head of the outgoing queue
     *
     * @return The message at the head of the incoming queue
     */
    public UserMessage getInMessage();

    public void setParentPortal(IPortal portal);

    public IPortal getParentPortal();
}
