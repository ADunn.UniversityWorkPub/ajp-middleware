/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.mas.common.IMASName;
import org.mas.message.user.UserMessage;
import org.mas.portal.IPortal;

/**
 * Implements a message agent
 *
 * @author Adam Dunn
 */
public class Agent implements IAgent, IMessageAnnouncer, IMASName
{

    private final ConcurrentLinkedQueue<UserMessage> outgoingQueue;
    private final ConcurrentLinkedQueue<UserMessage> incommingQueue;
    private final List<IMessageListener> listeners;
    private final Timer poller;
    private String name;
    private IPortal portal;

    /**
     * Initialise agent queues
     */
    public Agent()
    {
        outgoingQueue = new ConcurrentLinkedQueue<>();
        incommingQueue = new ConcurrentLinkedQueue<>();
        listeners = new ArrayList<>();

        poller = new Timer(true);
        poller.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                UserMessage msg;
                //Run until outgoing is empty
                while (portal != null && !outgoingQueue.isEmpty())
                {
                    msg = outgoingQueue.poll();
                    portal.sendUserMessage(msg);
                }
            }
        }, 200, 200);
    }

    /**
     * @see
     * org.mas.agent.IMessageAnnouncer#registerListener(org.mas.agent.IMessageListener)
     */
    @Override
    public boolean registerListener(IMessageListener listener)
    {
        if (!listeners.contains(listener))
        {
            return listeners.add(listener);
        }
        return true;
    }

    /**
     * @see
     * org.mas.agent.IMessageAnnouncer#deregisterListener(org.mas.agent.IMessageListener)
     */
    @Override
    public boolean deregisterListener(IMessageListener listener)
    {
        return listeners.remove(listener);
    }

    /**
     * @see org.mas.agent.IAgent#putOutMessage(org.mas.message.user.UserMessage)
     */
    @Override
    public boolean putOutMessage(UserMessage msg)
    {
        return outgoingQueue.add(msg);
    }

    /**
     * @see org.mas.agent.IAgent#getOutMessage()
     */
    @Override
    public UserMessage getOutMessage()
    {
        return outgoingQueue.poll();
    }

    /**
     * @see org.mas.agent.IAgent#putInMessage(org.mas.message.user.UserMessage)
     */
    @Override
    public boolean putInMessage(UserMessage msg)
    {
        alertAllListeners(msg);
        return incommingQueue.add(msg);
    }

    /**
     * @see org.mas.agent.IAgent#getInMessage()
     */
    @Override
    public UserMessage getInMessage()
    {
        return incommingQueue.poll();
    }

    @Override
    public String getMasName()
    {
        return name;
    }

    @Override
    public void setMasName(String name)
    {
        this.name = name;
    }

    @Override
    public void setParentPortal(IPortal portal)
    {
        this.portal = portal;
    }

    @Override
    public IPortal getParentPortal()
    {
        return portal;
    }

    /**
     * Sends a user message to all registered listeners
     */
    private void alertAllListeners(UserMessage msg)
    {
        listeners.forEach(l -> l.acceptMessage(msg));
    }
}
