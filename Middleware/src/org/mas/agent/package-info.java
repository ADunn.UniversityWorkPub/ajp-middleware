/*
 * SPDX-License-Identifier: Apache-2.0
 */
/**
 * Classes used to create and initialise agents
 */
package org.mas.agent;
