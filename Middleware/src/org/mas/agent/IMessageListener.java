/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.agent;

import org.mas.message.user.UserMessage;

/**
 * A message listener
 *
 * @author Adam Dunn
 */
public interface IMessageListener
{

    /**
     * Task to complete when a message is received
     *
     * @param message The new message
     */
    public void acceptMessage(UserMessage message);
}
