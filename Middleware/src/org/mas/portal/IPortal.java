/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.portal;

import org.mas.agent.IAgent;
import org.mas.common.IMASName;
import org.mas.common.node.INodeAnnouncer;
import org.mas.message.system.SystemMessage;
import org.mas.message.user.UserMessage;

/**
 * Portals must be able to register agents, handle system messages and pass on
 * user messages
 *
 * @author Adam Dunn
 */
public interface IPortal extends IMASName, INodeAnnouncer
{

    /**
     * Makes a portal aware of an agent
     *
     * @param name  The name given to the agent, name is used by other agents to
     *              address agent
     * @param agent The agent to register with the portal
     * @return True if the agent was successfully registered
     */
    public boolean registerAgent(String name, IAgent agent);

    /**
     * Removes an agent from the portal
     *
     * @param name The name of the agent to remove
     * @return True if the agent was removed
     */
    public boolean deregisterAgent(String name);

    public String[] listAgents();

    /**
     * Processes system message types or forwards them on
     *
     * @param sysMsg The message to handle
     */
    public void handleSystemMessage(SystemMessage sysMsg);

    /**
     * Sends a user message to the correct destination
     *
     * @param msg The message to send
     */
    public void sendUserMessage(UserMessage msg);
}
