/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.portal;

import org.mas.sockets.client.ClientSocket;

/**
 * Indicates a portal which capable of networking with a router
 *
 * @author Adam Dunn
 */
public interface IRoutedPortal extends IPortal
{

    public void setRouter(ClientSocket socket);

    public ClientSocket getRouter();

    public String getRouterMasName();
}
