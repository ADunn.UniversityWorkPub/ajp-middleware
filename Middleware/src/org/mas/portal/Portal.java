/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.portal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.mas.agent.IAgent;
import org.mas.common.IMASName;
import org.mas.common.address.MasAddress;
import org.mas.common.node.INodeListener;
import org.mas.message.Message;
import org.mas.message.system.SystemMessage;
import org.mas.message.user.UserMessage;
import org.mas.sockets.client.ClientSocket;

/**
 * Tracks locally registered agents and forms connections to routers via sockets
 *
 * @author Adam Dunn
 * @author Sam Moore
 * @author Szymon Kowalski
 */
public class Portal implements IRoutedPortal
{

    private final HashMap<String, IAgent> agents = new HashMap<>();
    private final List<INodeListener> listeners = new ArrayList<>();
    private ClientSocket routerConnection;
    private String masName;

    /**
     * Creates a basic portal with a random name
     */
    public Portal()
    {
        masName = "Portal - " + IMASName.getRandomChars();
    }

    @Override
    public boolean registerAgent(String name, IAgent agent)
    {
        agents.put(name, agent);
        agent.setParentPortal(this);

        System.out.println("Agent " + name + " has been added!");
        return true;
    }

    @Override
    public boolean deregisterAgent(String name)
    {
        IAgent a = agents.remove(name);

        a.setParentPortal(null);

        System.out.println("Agent " + this.masName + " has been removed!");
        return true;
    }

    @Override
    public String[] listAgents()
    {
        return agents.keySet().
                toArray(new String[0]);
    }

    @Override
    public void handleSystemMessage(SystemMessage sysMsg)
    {
        switch (sysMsg.getOperation())
        {
        }
    }

    @Override
    public void sendUserMessage(UserMessage msg)
    {
        alertNodeListeners(msg);

        MasAddress dst = msg.getDestination();

        if (dst.getPortal() == null || dst.getPortal().
                equals(masName))
        {
            IAgent a = agents.get(dst.getAgent());

            if (a != null)
            {
                a.putInMessage(msg);
            }
        }
        //Drop any message which doesn't match router's name since the address is invalid
        else if (dst.getRouter().
                equals(getRouterMasName()) && routerConnection != null)
        {
            routerConnection.enqueueMessage(msg);
        }
    }

    @Override
    public String getMasName()
    {
        return this.masName;
    }

    @Override
    public void setMasName(String name)
    {
        System.out.
                println("Agent " + this.masName + " changed name to " + name + ".");
        this.masName = name;
    }

    @Override
    public void setRouter(ClientSocket socket)
    {
        routerConnection = socket;
    }

    @Override
    public ClientSocket getRouter()
    {
        return routerConnection;
    }

    @Override
    public String getRouterMasName()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean registerNodeListener(INodeListener listener)
    {
        return listeners.add(listener);
    }

    @Override
    public boolean deregisterNodeListener(INodeListener listener)
    {
        return listeners.remove(listener);
    }

    private void alertNodeListeners(Message msg)
    {
        listeners.forEach(listener ->
        {
            listener.receiveNodeMessage(this, msg);
        });
    }
}
