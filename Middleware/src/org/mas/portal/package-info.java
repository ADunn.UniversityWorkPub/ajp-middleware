/*
 * SPDX-License-Identifier: Apache-2.0
 */
/**
 * Classes for creating portals and portal sockets
 */
package org.mas.portal;
