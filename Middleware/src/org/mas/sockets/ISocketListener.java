/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.sockets;

import org.mas.message.Message;

/**
 * Method all socket listeners must implement
 *
 * @author Adam Dunn
 */
public interface ISocketListener
{

    /**
     * An incoming message is passed to listeners when received
     *
     * @param msg The message
     */
    public void receiveMessage(Message msg);
}
