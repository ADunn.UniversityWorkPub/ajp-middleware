/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.sockets;

/**
 * Methods all socket announcers must implement
 *
 * @author Adam Dunn
 */
public interface ISocketAnnouncer
{

    /**
     * Add a new listener to be alerted of incoming messages
     *
     * @param listener
     */
    public void registerSocketListener(ISocketListener listener);

    /**
     * Remove a listener from being alerted to new messages
     *
     * @param listener
     */
    public void deregisterSocketListener(ISocketListener listener);
}
