/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.sockets.router;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mas.message.Message;
import org.mas.sockets.ISocketAnnouncer;
import org.mas.sockets.ISocketListener;
import org.mas.sockets.client.ClientSocket;

/**
 * Implements the server side socket to be used by a router
 *
 * @author Adam Dunn
 */
public class RouterServerSocket implements ISocketAnnouncer
{

    private final int port;
    private ServerSocket socket;
    private PrintWriter outStream;
    private BufferedReader inStream;

    private final List<ClientSocket> remotes;
    private final ConcurrentLinkedQueue<Message> outgoing;

    private final List<ISocketListener> listeners;

    /**
     * Sets up a new server socket
     *
     * @param port The port to listen to, must be greater than 1000 to avoid
     *             requiring root user
     */
    public RouterServerSocket(int port)
    {
        remotes = new CopyOnWriteArrayList<>();
        outgoing = new ConcurrentLinkedQueue<>();
        listeners = new ArrayList<>();
        this.port = port;

        ExecutorService outGoingPoller = Executors.newScheduledThreadPool(1);
        ((ScheduledExecutorService) outGoingPoller).
                scheduleAtFixedRate(() ->
                {
                    Message m = outgoing.poll();

                    if (m != null)
                    {
                        String portal = m.getDestination().
                                getPortal();

                        remotes.forEach(remoteSocket ->
                        {
                            if (remoteSocket.getMasName().
                                    equals(portal))
                            {
                                remoteSocket.enqueueMessage(m);
                            }
                        });
                    }
                }, 200l, 200l, TimeUnit.MILLISECONDS);
    }

    /**
     * Attempt to open socket and attach an auto accepting background thread
     *
     * @return
     */
    public boolean initSocket()
    {
        try
        {
            socket = new ServerSocket(port);

            //Handle 10 concurrent connections at once
            ExecutorService bg = Executors.newFixedThreadPool(10);
            bg.submit(() ->
            {
                while (true)
                {
                    Socket s = socket.accept();
                    ClientSocket cs = new ClientSocket(s);
                    cs.start();

                    listeners.forEach(listener ->
                    {
                        cs.registerSocketListener(listener);
                    });

                    remotes.add(cs);
                }
            });
        }
        catch (IOException ex)
        {
            Logger.getLogger(RouterServerSocket.class.getName()).
                    log(Level.SEVERE, "Failed to create router server socket", ex);
            return false;
        }

        return true;
    }

    public void enqueueOutgoingMessage(Message msg)
    {
        outgoing.add(msg);
    }

    @Override
    public void registerSocketListener(ISocketListener listener)
    {
        listeners.add(listener);
    }

    @Override
    public void deregisterSocketListener(ISocketListener listener)
    {
        listeners.remove(listener);
    }
}
