/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.sockets.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mas.common.IMASName;
import org.mas.message.Message;
import org.mas.sockets.ISocketAnnouncer;
import org.mas.sockets.ISocketListener;

/**
 * Creates a send receive socket to a portal
 *
 * @author Adam Dunn
 */
public class ClientSocket implements ISocketAnnouncer, IMASName
{

    private final InetAddress remote;
    private final int port;

    private String masName;
    private Socket outSocket;

    private PrintWriter outStream;
    private BufferedReader inStream;

    private boolean ready;

    private final List<ISocketListener> listeners;
    private final ConcurrentLinkedQueue<Message> incoming;
    private final ConcurrentLinkedQueue<Message> outgoing;

    private final ExecutorService poll;
    private final SocketPoller poller;

    /**
     * Creates a new instance of ClientSocket
     *
     * @param remoteAddress
     * @param remotePort
     */
    public ClientSocket(InetAddress remoteAddress, int remotePort)
    {
        listeners = new ArrayList<>();
        incoming = new ConcurrentLinkedQueue<>();
        outgoing = new ConcurrentLinkedQueue<>();
        poll = Executors.newScheduledThreadPool(1);
        poller = new SocketPoller(inStream, incoming);

        remote = remoteAddress;
        port = remotePort;

        ready = false;
    }

    /**
     * Encapsulate a preexisting socket
     *
     * @param socket
     */
    public ClientSocket(Socket socket)
    {
        listeners = new ArrayList<>();
        incoming = new ConcurrentLinkedQueue<>();
        outgoing = new ConcurrentLinkedQueue<>();
        poll = Executors.newScheduledThreadPool(1);
        poller = new SocketPoller(inStream, incoming);

        outSocket = socket;
        remote = socket.getInetAddress();
        port = socket.getPort();

        ready = true;
    }

    /**
     * Attempts to start the socket
     *
     * @return True If socket is functioning correctly
     */
    public boolean start()
    {
        boolean result = initSocketStreams();

        if (!result)
        {
            try
            {
                outSocket.close();
            }
            catch (NullPointerException | IOException e)
            {
                outSocket = null;
            }
        }
        else
        {
            //Start polling for new messages
            ((ScheduledExecutorService) poll).
                    scheduleAtFixedRate(poller, 200l, 200l, TimeUnit.MILLISECONDS);
        }

        ready = result;

        return ready;
    }

    /**
     * Send message stub
     *
     * @param msg The message object
     * @return True if the message was sent
     */
    public boolean enqueueMessage(Message msg)
    {
        return outgoing.add(msg);
    }

    /**
     * Add an object to alert when a new message is received
     *
     * @param listener The object to alert
     */
    @Override
    public void registerSocketListener(ISocketListener listener)
    {
        if (!listeners.contains(listener))
        {
            listeners.add(listener);
        }
    }

    /**
     * Remove an object from the list of object to alert
     *
     * @param listener The listener to remove
     */
    @Override
    public void deregisterSocketListener(ISocketListener listener)
    {
        listeners.remove(listener);
    }

    /**
     * @return True if incoming and outgoing sockets are setup
     */
    public synchronized boolean isReady()
    {
        return ready;
    }

    @Override
    public String getMasName()
    {
        return masName;
    }

    @Override
    public void setMasName(String name)
    {
        this.masName = name;
    }

    private void alertAllListeners(Message msg)
    {
        listeners.forEach(l -> l.receiveMessage(msg));
    }

    /**
     * Initialise socket and stream reader / writer
     *
     * @return
     */
    private boolean initSocketStreams()
    {
        try
        {
            if (outSocket == null)
            {
                outSocket = new Socket(remote, port);
            }

            outStream = new PrintWriter(outSocket.getOutputStream(), true);
            inStream = new BufferedReader(new InputStreamReader(outSocket.
                    getInputStream()));
            return true;
        }
        catch (IOException ex)
        {
            Logger.getLogger(ClientSocket.class.getName()).
                    log(Level.SEVERE, "IOException when initialising outgoing socket", ex);
            return false;
        }
    }

    /**
     * Takes the next message in the queue and sends it
     */
    private void sendMessage()
    {
        Message message = outgoing.poll();

        if (message != null)
        {
            String json = message.toJSON();
            outStream.println(json);
        }

    }
}
