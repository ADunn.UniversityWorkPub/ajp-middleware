/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.sockets.client;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mas.message.Message;

/**
 * A class which polls a socket's incoming stream
 *
 * @author Adam Dunn
 */
public class SocketPoller implements Runnable
{

    private final BufferedReader input;
    private final ConcurrentLinkedQueue<Message> incoming;

    public SocketPoller(BufferedReader inStream, ConcurrentLinkedQueue<Message> in)
    {
        input = inStream;
        incoming = in;
    }

    /**
     * Attempts to read a message from the input stream
     *
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run()
    {
        try
        {
            //Only build a message if the reader is ready
            if (input.ready())
            {
                String JSON = input.readLine();
                Message msg = new Gson().fromJson(JSON, Message.class);

                incoming.add(msg);
            }
        }
        catch (IOException ex)
        {
            Logger.getLogger(SocketPoller.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        catch (NullPointerException npe)
        {
            //Do nothing
        }
    }

}
