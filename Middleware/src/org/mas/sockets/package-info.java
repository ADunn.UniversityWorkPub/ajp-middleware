/*
 * SPDX-License-Identifier: Apache-2.0
 */
/**
 * Classes related to building sockets
 */
package org.mas.sockets;
