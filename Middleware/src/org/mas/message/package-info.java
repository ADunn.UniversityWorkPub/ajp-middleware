/*
 * SPDX-License-Identifier: Apache-2.0
 */
/**
 * Classes for defining and creating messages
 */
package org.mas.message;
