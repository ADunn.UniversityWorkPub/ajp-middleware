/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.message;

import com.google.gson.Gson;
import org.mas.common.address.MasAddress;

/**
 * Defines the structure of all messages
 *
 * @author Adam Dunn
 */
public abstract class Message
{

    private MasAddress destination;
    private MasAddress source;
    private int ttl;

    protected Message(MasAddress destination, MasAddress source)
    {
        ttl = 10;
        this.destination = destination;
        this.source = source;
    }

    public MasAddress getDestination()
    {
        return destination;
    }

    public void setDestination(MasAddress destination)
    {
        this.destination = destination;
    }

    public MasAddress getSource()
    {
        return source;
    }

    public void setSource(MasAddress source)
    {
        this.source = source;
    }

    public int getTtl()
    {
        return ttl;
    }

    public int decrementTtl()
    {
        ttl = ttl - 1;
        return ttl;
    }

    /**
     * Serialize this message into JSON
     *
     * @return This object expressed as JSON
     */
    public String toJSON()
    {
        Gson parser = new Gson();
        String json = parser.toJson(this);
        return json;
    }
}
