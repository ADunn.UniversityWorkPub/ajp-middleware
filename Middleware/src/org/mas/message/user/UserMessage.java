/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.message.user;

import org.mas.common.address.MasAddress;
import org.mas.message.Message;

/**
 *
 * @author Adam Dunn
 * @param <T> The type of object to send
 */
public class UserMessage<T> extends Message
{

    private final T message;

    public UserMessage(T message, MasAddress destination, MasAddress source)
    {
        super(destination, source);
        this.message = message;

    }

    public Object getMessage()
    {
        return message;
    }

}
