/*
 * SPDX-License-Identifier: Apache-2.0
 */
/**
 * User message classes
 */
package org.mas.message.user;
