/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.message.system;

/**
 * MAS Remote Protocol
 *
 * @author Adam Dunn
 */
public enum SystemMessageType
{
    /**
     * Addressed - Used to identify the name of a portal / router
     */
    WHOIS,
    /**
     * Broadcast - Informs routers of a newly connected agent
     */
    JOINING,
    /**
     * Broadcast - Informs routers that an agent is disconnecting
     */
    LEAVING,
    /**
     * Addressed - Sends a message to a portal to test a socket connection
     */
    PING,
    /**
     * Addressed - Generic type used to respond to requests
     */
    RESPONSE
}
