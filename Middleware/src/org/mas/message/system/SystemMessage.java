/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.message.system;

import com.google.gson.Gson;
import org.mas.common.address.MasAddress;
import org.mas.message.Message;

/**
 * Messages used by portals and routers
 *
 * @author Adam Dunn
 */
public class SystemMessage extends Message
{

    private final SystemMessageType operation;
    private final String parameter;

    public SystemMessage(SystemMessageType operation, String parameter, MasAddress destination, MasAddress source)
    {
        super(destination, source);
        this.operation = operation;
        this.parameter = parameter;
    }

    public SystemMessageType getOperation()
    {
        return operation;
    }

    public String getParameter()
    {
        return parameter;
    }

    /**
     * @see org.mas.message.Message#toJSON()
     */
    @Override
    public String toJSON()
    {
        Gson parser = new Gson();
        String json = parser.toJson(this);
        return json;
    }
}
