/*
 * SPDX-License-Identifier: Apache-2.0
 */
/**
 * Classes related to system messages and their types
 */
package org.mas.message.system;
