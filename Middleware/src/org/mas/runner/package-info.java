/*
 * SPDX-License-Identifier: Apache-2.0
 */
/**
 * Classes used to handle the library being launched as a standalone app
 */
package org.mas.runner;
