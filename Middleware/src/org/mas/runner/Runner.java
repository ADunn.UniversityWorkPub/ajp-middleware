/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.runner;

/**
 * A driver class to alert a user when they incorrectly use the library
 *
 * @author Adam Dunn
 */
public class Runner
{

    public static void main(String[] args)
    {
        System.out.
                println("Warning! This JAR is a library and cannot be run standalone");
    }
}
