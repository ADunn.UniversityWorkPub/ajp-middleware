/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.common.address;

/**
 * Stores an address to use for routing
 *
 * @author Adam Dunn
 */
public class MasAddress
{

    private final String router, portal, agent;

    /**
     * Create a new fully qualified address
     *
     * @param router The router the destination is on
     * @param portal The Portal the destination is connected to
     * @param agent  The Agent at the destination
     */
    public MasAddress(String router, String portal, String agent)
    {
        this.router = router;
        this.portal = portal;
        this.agent = agent;
    }

    public MasAddress(String portal, String agent)
    {
        this.router = null;
        this.portal = portal;
        this.agent = agent;
    }

    public MasAddress(String agent)
    {
        this.router = null;
        this.portal = null;
        this.agent = agent;
    }

    public String getRouter()
    {
        return router;
    }

    public String getPortal()
    {
        return portal;
    }

    public String getAgent()
    {
        return agent;
    }

}
