/*
 * SPDX-License-Identifier: Apache-2.0
 */
/**
 * Classes to build and parse MAS addresses
 */
package org.mas.common.address;
