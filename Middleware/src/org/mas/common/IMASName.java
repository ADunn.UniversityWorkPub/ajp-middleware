/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.common;

import java.util.Random;

/**
 * Any object implementing IMASName must have an addressable name to be used by
 * other agents, portals and routers
 *
 * @author Adam Dunn
 */
public interface IMASName
{

    /**
     * Gets the node's name
     *
     * @return The name
     */
    public String getMasName();

    /**
     * Sets the node's name
     *
     * @param name The name
     */
    public void setMasName(String name);

    /**
     * Produces a set of random letters, useful for auto generating a mas name
     *
     * @return 8 random letters
     */
    public static String getRandomChars()
    {
        Random r = new Random();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 8; i++)
        {
            char next = (char) (r.nextInt(26) + 'A');
            sb.append(next);
        }

        return sb.toString();
    }
}
