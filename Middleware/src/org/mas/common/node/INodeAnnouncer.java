/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.common.node;

import org.mas.common.IMASName;

/**
 * INodeAnnouncers are useful for debugging, they will announce messages they
 * receive or transmit
 *
 * @author Adam Dunn
 */
public interface INodeAnnouncer extends IMASName
{

    public boolean registerNodeListener(INodeListener listener);

    public boolean deregisterNodeListener(INodeListener listener);

}
