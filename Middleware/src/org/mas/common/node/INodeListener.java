/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.common.node;

import org.mas.message.Message;

/**
 * Allows classes to receive debug information from INodeAnnouncers
 *
 * @author Adam Dunn
 */
public interface INodeListener
{

    public void receiveNodeMessage(INodeAnnouncer src, Message msg);
}
