/*
 * SPDX-License-Identifier: Apache-2.0
 */
/**
 * Node manager interfaces
 */
package org.mas.common.node;
