/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.common;

import java.util.concurrent.ConcurrentLinkedQueue;
import org.mas.message.user.UserMessage;
import org.mas.portal.IPortal;

/**
 *
 * @author adunn
 */
public class ThreadedQueueHandler implements Runnable
{

    private final ConcurrentLinkedQueue<UserMessage> outgoing;
    private final IPortal portal;

    private boolean exec;

    public ThreadedQueueHandler(IPortal portal, ConcurrentLinkedQueue<UserMessage> out)
    {
        outgoing = out;
        this.portal = portal;
        exec = true;
    }

    @Override
    public void run()
    {
        while (exec)
        {
            UserMessage next = outgoing.poll();
            if (next != null)
            {
                portal.sendUserMessage(next);
            }

            try
            {
                Thread.sleep(200l);
            }
            catch (InterruptedException e)
            {
                //Restart thread
            }
        }
    }

    public boolean isExec()
    {
        return exec;
    }

    public void setExec(boolean exec)
    {
        this.exec = exec;
    }

}
