/*
 * SPDX-License-Identifier: Apache-2.0
 */
/**
 * Utility classes and shared functionality
 */
package org.mas.common;
