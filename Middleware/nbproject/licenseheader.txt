<#if licenseFirst??>
${licenseFirst}
</#if>
${licensePrefix}SPDX-License-Identifier: Apache-2.0
<#if licenseLast??>
${licenseLast}
</#if>