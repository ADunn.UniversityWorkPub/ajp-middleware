/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.exercise;

import java.util.Scanner;
import org.mas.exercise.agent.AgentManager;
import org.mas.exercise.portal.PortalCreator;
import org.mas.exercise.router.RouterCreator;
import org.mas.router.ServedRouter;

/**
 *
 * @author Adam Dunn
 */
public class Exercise {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        int option;

        do {
            option = menuOption();

            switch (option) {
                case 1:
                    option1();
                    break;
                case 2:
                    option2();
                    break;
                case 3:
                    option3();
                    break;
                case 4:
                    option4();
                    break;
                default:
                    break;
            }
        } while (option != 0);

        System.exit(0);
    }

    private static int menuOption() {
        int selection = -1;

        while (selection > 4 || selection < 0) {
            System.out.println("\nSelect an operation: ");
            System.out.println("\t1 - Create Router");
            System.out.println("\t2 - Create Portal");
            System.out.println("\t3 - Manage Agents");
            System.out.println("\t4 - List Nodes");
            System.out.println("\t0 - Quit");
            System.out.println("");
            System.out.print("Enter: ");

            selection = Integer.parseInt(SCANNER.nextLine());
        }

        return selection;
    }

    private static void option1() {
        RouterCreator.create();
    }

    private static void option2() {
        PortalCreator.createPortal();
    }

    private static void option3() {
        AgentManager.main();
    }

    private static void option4() {
        ServedRouter r = RouterCreator.get();

        if (r != null) {
            System.out.println("Router - " + r.getMasName());
        }

        System.out.println("");

        for (String portal : PortalCreator.listPortals()) {
            System.out.println("Portal - " + PortalCreator.getPortal(portal).getMasName());
        }
    }

}
