/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.exercise.router;

import org.mas.router.ServedRouter;

/**
 *
 * @author Adam Dunn
 */
public class RouterCreator {

    private static ServedRouter router;

    public static void create() {
        if (router == null) {
            router = new ServedRouter();
        }
    }

    public static ServedRouter get() {
        return router;
    }
}
