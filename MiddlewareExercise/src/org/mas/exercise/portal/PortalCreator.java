/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.exercise.portal;

import java.util.HashMap;
import java.util.Map;
import org.mas.portal.IPortal;
import org.mas.portal.Portal;

/**
 * Creates portals to test them
 *
 * @author Adam Dunn
 */
public class PortalCreator {

    private static final Map<String, IPortal> PORTALS = new HashMap();

    public static void createPortal(String name) {
        Portal p = new Portal();
        p.setMasName(name);

        p.registerNodeListener((src, msg) -> {
            System.out.println("Portal " + src.getMasName() + " recevied message from " + msg.getSource().getAgent());
        });

        PORTALS.put(name, new Portal());
    }

    public static void createPortal() {
        Portal p = new Portal();
        PORTALS.put(p.getMasName(), p);
    }

    public static String[] listPortals() {
        return PORTALS.keySet().toArray(new String[0]);
    }

    public static IPortal getPortal(String portal) {
        return PORTALS.get(portal);
    }

}
