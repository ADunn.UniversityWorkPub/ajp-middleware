/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.exercise.agent;

import java.util.Scanner;
import org.mas.exercise.portal.PortalCreator;
import org.mas.portal.IPortal;

/**
 *
 * @author adunn
 */
public class AgentManager {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main() {
        System.out.println("Select an option: ");
        System.out.println("\t1 - Create Agents");
        System.out.println("\t2 - Start message sending");
        System.out.println("\t3 - Stop message sending");

        int sel = Integer.parseInt(SCANNER.nextLine());
        switch (sel) {
            case 1:
                createAgents();
                break;
            case 2:
                startSend();
                break;
            case 3:
                stopSend();
                break;
        }
    }

    private static void createAgents() {
        System.out.println("\nSelect a portal to add agents to");

        for (int i = 0; i < PortalCreator.listPortals().length; i++) {
            IPortal p = PortalCreator.getPortal(PortalCreator.listPortals()[i]);
            System.out.println("\t Portal #" + i + " (" + p.getMasName() + ") - Agents: " + p.listAgents().length);
        }

        int sel = Integer.parseInt(SCANNER.nextLine());

        IPortal p = PortalCreator.getPortal(PortalCreator.listPortals()[sel]);

        System.out.print("How many agents to create: ");
        int num = Integer.parseInt(SCANNER.nextLine());

        for (int i = 0; i < num; i++) {
            AgentRunner a = AgentCreator.createAgent();
            p.registerAgent(a.getAgent().getMasName(), a.getAgent());
        }
    }

    private static void startSend() {
        System.out.println("Agents will now randomly send each other messages");

        AgentCreator.randomSendStart();
    }

    private static void stopSend() {
        System.out.println("Agents will no longer randomly send messages");

        AgentCreator.randomSendStop();
    }
}
