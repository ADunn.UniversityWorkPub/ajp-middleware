/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.exercise.agent;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mas.agent.Agent;
import org.mas.agent.IMessageListener;
import org.mas.common.address.MasAddress;
import org.mas.message.user.UserMessage;

/**
 *
 * @author adunn
 */
public class AgentRunner implements IMessageListener {

    private final Agent agent = new Agent();
    private Timer sender;

    public AgentRunner() {
        agent.registerListener(this::acceptMessage);
    }

    public Agent getAgent() {
        return agent;
    }

    public void startMessageSend(MasAddress address) {
        stopMessageSend();
        sender = new Timer();

        sender.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                UserMessage<String> msg = new UserMessage<>("Test message", address, new MasAddress(agent.getMasName()));
                agent.putOutMessage(msg);
            }
        }, 500, 500);
    }

    public void stopMessageSend() {
        if (sender != null) {
            sender.cancel();
            sender = null;
        }
    }

    @Override
    public void acceptMessage(UserMessage message) {
        //System.out.println(agent.getMasName() + " received message from " + message.getSource().getAgent());

        try {
            FileWriter fw = new FileWriter(agent.getMasName() + ".msglog", true);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.append("\n");
            bw.append(message.toJSON());

            bw.close();
            fw.close();

        } catch (IOException ex) {
            Logger.getLogger(AgentRunner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
