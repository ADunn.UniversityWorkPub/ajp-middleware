/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.mas.exercise.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.mas.common.IMASName;
import org.mas.common.address.MasAddress;

/**
 *
 * @author adunn
 */
public class AgentCreator {

    private static final List<AgentRunner> AGENTS = new ArrayList<>();

    public static AgentRunner createAgent() {
        AgentRunner a = new AgentRunner();
        a.getAgent().setMasName("Agent - " + IMASName.getRandomChars());
        AGENTS.add(a);
        return a;
    }

    public static void randomSendStart() {
        Random r = new Random();

        AGENTS.forEach((ar) -> {
            String dest = AGENTS.get(r.nextInt(AGENTS.size()))
                    .getAgent().getMasName();

            ar.startMessageSend(new MasAddress(dest));
        });
    }

    public static void randomSendStop() {
        AGENTS.forEach((ar) -> {
            ar.stopMessageSend();
        });
    }
}
